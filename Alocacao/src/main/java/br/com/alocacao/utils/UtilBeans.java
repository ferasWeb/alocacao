/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.alocacao.utils;

/**
 *
 * @author arthur.diego
 */
public class UtilBeans {
    /**
     * Nome da persistência de Unidade
     */
    public static final String PERSISTENCE_UNIT = "alocacaoPU";
    /**
     * Define qual Banco de Dados aplicação está utilizando.
     */
    public static final String DATABASE_NAME = "alocacao";
    /**
     * Define qual o nome do servidor de Banco de Dados a aplicação está utilizando.
     */
    public static final String SERVER_NAME = "localhost";
    /**
     * Define qual Usuário tem acesso ao banco de dados.
     */
    public static final String DATABASE_USER = "root";
    /**
     * Define qual a porta de conexão do Banco de Dados.
     */
    public static final String PORT = "3306";
    /**
     * Define a Senha para conexão com o banco de dados. Senha referente ao
     * DATABASE_USER.
     */ 
    public static final String DATABASE_PASS = "1234";
}
