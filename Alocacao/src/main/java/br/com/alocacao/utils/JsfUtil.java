package br.com.alocacao.utils;

import br.com.alocacao.model.common.EntityInterface;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;


/**
 * Classe utilitaria para trabalhar com JSF.
 */
public class JsfUtil {
    
   /**
     * String que representa o retorno para ficar na mesma página.
     */
    public static final String MANTEM = "";
    
    
    /**
     * Criado pelo IDE esse método retorna um Array de SelectItem que você
     * utiliza para seleção de Objetos, funciona com entidades que implementam
     * EntityInterface.
     *
     * @param entities Uma lista de EntityInterface
     * @param selectOne True acrescenta a opção Selecione, false somente opções
     * da entidade são exibidas.
     * @param contex Contexto JSF
     * @return Array de SelectItem[] para ser utilizado na tag f:selectItems
     */
    public static SelectItem[] getSelectItems(List<? extends EntityInterface> entities, boolean selectOne) {
        int size = selectOne ? entities.size() + 1 : entities.size();
        SelectItem[] items = new SelectItem[size];
        int i = 0;
        if (selectOne) {
            items[0] = new SelectItem("", "---");
            i++;
        }
        for (EntityInterface x : entities) {
            items[i++] = new SelectItem(x, x.getLabel());
        }
        return items;
    }

    public static void addErrorMessage(Exception ex, String defaultMsg) {
        String msg = ex.getLocalizedMessage();
        if (msg != null && msg.length() > 0) {
            addErrorMessage(msg);
        } else {
            addErrorMessage(defaultMsg);
        }
    }

    public static void addErrorMessages(List<String> messages) {
        for (String message : messages) {
            addErrorMessage(message);
        }
    }

    public static void addErrorMessage(String msg) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
        FacesContext.getCurrentInstance().addMessage(null, facesMsg);
    }

    public static void addSuccessMessage(String msg) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg);
        FacesContext.getCurrentInstance().addMessage("successInfo", facesMsg);
    }

    public static String getRequestParameter(String key) {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(key);
    }

    public static Object getObjectFromRequestParameter(String requestParameterName, Converter converter, UIComponent component) {
        String theId = JsfUtil.getRequestParameter(requestParameterName);
        return converter.getAsObject(FacesContext.getCurrentInstance(), component, theId);
    }

}
