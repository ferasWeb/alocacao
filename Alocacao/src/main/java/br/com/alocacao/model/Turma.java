package br.com.alocacao.model;

import br.com.alocacao.model.common.EntityInterface;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Turma")
public class Turma implements EntityInterface<Turma> {

    @Id
    @GeneratedValue
    private Long idTurma;

    @Column(name = "turma", nullable = false, unique = true)
    private String nome_turma;

    public Turma() {
    }

    public Long getIdTurma() {
        return idTurma;
    }

    public void setIdTurma(Long idTurma) {
        this.idTurma = idTurma;
    }

    public String getNome_turma() {
        return nome_turma;
    }

    public void setNome_turma(String nome_turma) {
        this.nome_turma = nome_turma;
    }

    @Override
    public String getLabel() {
        return this.getIdTurma() + " - " + this.getNome_turma();
    }

    @Override
    public Long getId() {
        return this.getIdTurma();
    }

    @Override
    public int compareTo(Turma o) {
        return this.getIdTurma().compareTo(o.getIdTurma());
    }

    @Override
    public String toString() {
        return "Turma{" + "idTurma=" + idTurma + ", nome_turma=" + nome_turma + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.idTurma);
        hash = 73 * hash + Objects.hashCode(this.nome_turma);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Turma other = (Turma) obj;
        if (!Objects.equals(this.idTurma, other.idTurma)) {
            return false;
        }
        if (!Objects.equals(this.nome_turma, other.nome_turma)) {
            return false;
        }
        return true;
    }

}
