/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alocacao.model;

import br.com.alocacao.model.common.EntityInterface;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author arthur.diego
 */
@Entity
@Table(name = "Alocacao")
public class Alocacao implements EntityInterface<Alocacao> {

    @Id
    @GeneratedValue
    private Long idAlocacao;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="id_sala", referencedColumnName = "idSala", nullable = false)
    private Sala sala;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="id_turma", referencedColumnName = "idTurma", nullable = false)
    private Turma turma;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="id_disciplina", referencedColumnName = "idDisponibilidade", nullable = false)
    private Disponibilidade dia;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="id_professor", referencedColumnName = "idProfessor", nullable = false)
    private Professor professor;

    public Alocacao() {
    }

    public Long getIdAlocacao() {
        return idAlocacao;
    }

    public void setIdAlocacao(Long idAlocacao) {
        this.idAlocacao = idAlocacao;
    }

    public Sala getSala() {
        return sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

    public Turma getTurma() {
        return turma;
    }

    public void setTurma(Turma turma) {
        this.turma = turma;
    }

    public Disponibilidade getDia() {
        return dia;
    }

    public void setDia(Disponibilidade dia) {
        this.dia = dia;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    @Override
    public int compareTo(Alocacao o) {
        return this.getIdAlocacao().compareTo(o.getIdAlocacao());
    }

    @Override
    public String getLabel() {
        return "";
    }

    @Override
    public Long getId() {
        return this.getIdAlocacao();
    }

   

}
