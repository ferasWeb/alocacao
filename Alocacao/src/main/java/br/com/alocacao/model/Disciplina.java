package br.com.alocacao.model;

import br.com.alocacao.enumerated.Disciplina_Tipo;
import br.com.alocacao.model.common.EntityInterface;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Disciplina")
public class Disciplina implements EntityInterface<Disciplina> {

    @Id
    @GeneratedValue
    private Long idDisciplina;

    @Column(name = "nome_disciplina", nullable = true, length = 100)
    private String nomeDisciplina;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_aula", nullable = false)
    private Disciplina_Tipo tipo;

    public Disciplina() {
    }

    public Long getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(Long idDisciplina) {
        this.idDisciplina = idDisciplina;
    }

    public String getNomeDisciplina() {
        return nomeDisciplina;
    }

    public void setNomeDisciplina(String nomeDisciplina) {
        this.nomeDisciplina = nomeDisciplina;
    }

    public Disciplina_Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Disciplina_Tipo tipo) {
        this.tipo = tipo;
    }

    @Override
    public String getLabel() {
        return this.getId() + " - " + this.getNomeDisciplina()+" - "+this.getTipo().getLabel();
    }

    @Override
    public Long getId() {
        return this.getIdDisciplina();
    }

    @Override
    public int compareTo(Disciplina o) {
        return this.getIdDisciplina().compareTo(o.getIdDisciplina());
    }

    @Override
    public String toString() {
        return "Disciplina{" + "idDisciplina=" + idDisciplina + ", nomeDisciplina=" + nomeDisciplina + ", tipo=" + tipo + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.idDisciplina);
        hash = 19 * hash + Objects.hashCode(this.nomeDisciplina);
        hash = 19 * hash + Objects.hashCode(this.tipo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Disciplina other = (Disciplina) obj;
        if (!Objects.equals(this.idDisciplina, other.idDisciplina)) {
            return false;
        }
        if (!Objects.equals(this.nomeDisciplina, other.nomeDisciplina)) {
            return false;
        }
        if (this.tipo != other.tipo) {
            return false;
        }
        return true;
    }

}
