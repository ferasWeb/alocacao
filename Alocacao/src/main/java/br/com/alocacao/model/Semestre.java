/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alocacao.model;

import br.com.alocacao.enumerated.Semestre_Tipo;
import br.com.alocacao.model.common.EntityInterface;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author arthur.diego
 */
@Entity
@Table(name = "Semestre")
public class Semestre implements EntityInterface<Semestre>{

    @Id
    @GeneratedValue
    private Long idSemestre;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "semestre", nullable = false)
    private Semestre_Tipo semestre;

    public Semestre() {
    }

    public Long getIdSemestre() {
        return idSemestre;
    }

    public void setIdSemestre(Long idSemestre) {
        this.idSemestre = idSemestre;
    }

    public Semestre_Tipo getSemestre() {
        return semestre;
    }

    public void setSemestre(Semestre_Tipo semestre) {
        this.semestre = semestre;
    }

    @Override
    public String getLabel() {
        return this.getId()+" - "+this.getSemestre().getLabel();
    }

    @Override
    public Long getId() {
        return this.getIdSemestre();
    }

    @Override
    public String toString() {
        return "Semestre{" + "idSemestre=" + idSemestre + ", semestre=" + semestre + '}';
    }
    
    @Override
    public int compareTo(Semestre o) {
        return this.getIdSemestre().compareTo(o.getIdSemestre());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.idSemestre);
        hash = 59 * hash + Objects.hashCode(this.semestre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Semestre other = (Semestre) obj;
        if (!Objects.equals(this.idSemestre, other.idSemestre)) {
            return false;
        }
        if (this.semestre != other.semestre) {
            return false;
        }
        return true;
    }
    
    
}
