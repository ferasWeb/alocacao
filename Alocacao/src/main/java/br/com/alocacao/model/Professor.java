package br.com.alocacao.model;

import br.com.alocacao.model.common.EntityInterface;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Professor")
public class Professor implements EntityInterface<Professor> {

    @Id
    @GeneratedValue
    private Long idProfessor;

    @Column(name = "nome_prof", nullable = true, length = 255)
    private String nomeProfessor;

    @OneToMany(mappedBy = "professor")
    private List<Disponibilidade> disponibilidade;

    public Professor() {
    }

    public Long getIdProfessor() {
        return idProfessor;
    }

    public void setIdProfessor(Long idProfessor) {
        this.idProfessor = idProfessor;
    }


    public String getNomeProfessor() {
        return nomeProfessor;
    }

    public void setNomeProfessor(String nomeProfessor) {
        this.nomeProfessor = nomeProfessor;
    }

    public List<Disponibilidade> getDisponibilidade() {
        return disponibilidade;
    }

    public void setDisponibilidade(List<Disponibilidade> disponibilidade) {
        this.disponibilidade = disponibilidade;
    }

    @Override
    public String getLabel() {
        return this.getId()+" - "+this.getNomeProfessor();
    }

    @Override
    public Long getId() {
        return this.getIdProfessor();
    }

    @Override
    public int compareTo(Professor o) {
        return this.getIdProfessor().compareTo(o.getIdProfessor());
    }

    @Override
    public String toString() {
        return "Professor{" + "idProfessor=" + idProfessor + ", nomeProfessor=" + nomeProfessor + ", disponibilidade=" + disponibilidade + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + Objects.hashCode(this.idProfessor);
        hash = 61 * hash + Objects.hashCode(this.nomeProfessor);
        hash = 61 * hash + Objects.hashCode(this.disponibilidade);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Professor other = (Professor) obj;
        if (!Objects.equals(this.idProfessor, other.idProfessor)) {
            return false;
        }
        if (!Objects.equals(this.nomeProfessor, other.nomeProfessor)) {
            return false;
        }
        if (!Objects.equals(this.disponibilidade, other.disponibilidade)) {
            return false;
        }
        return true;
    }
    
    
    
}   
