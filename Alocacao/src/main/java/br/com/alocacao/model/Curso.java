/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alocacao.model;

import br.com.alocacao.model.common.EntityInterface;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author arthur.diego
 */
@Entity
@Table(name = "Curso")
public class Curso implements EntityInterface<Curso> {

    @Id
    @GeneratedValue
    private Long idCurso;

    @Column(name = "curso", nullable = false)
    private String curso_nome;

    public Curso() {
    }

    public Long getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(Long idCurso) {
        this.idCurso = idCurso;
    }

    public String getCurso_nome() {
        return curso_nome;
    }

    public void setCurso_nome(String curso_nome) {
        this.curso_nome = curso_nome;
    }

    @Override
    public String getLabel() {
        return this.getIdCurso()+" - "+this.getCurso_nome();
    }

    @Override
    public Long getId() {
        return this.getIdCurso();
    }

    @Override
    public int compareTo(Curso o) {
        return this.getIdCurso().compareTo(o.getIdCurso());
    }

    @Override
    public String toString() {
        return "Curso{" + "idCurso=" + idCurso + ", curso_nome=" + curso_nome + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.idCurso);
        hash = 89 * hash + Objects.hashCode(this.curso_nome);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Curso other = (Curso) obj;
        if (!Objects.equals(this.idCurso, other.idCurso)) {
            return false;
        }
        if (!Objects.equals(this.curso_nome, other.curso_nome)) {
            return false;
        }
        return true;
    }
    
    

}
