/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.alocacao.model;

import br.com.alocacao.enumerated.Turno_Tipo;
import br.com.alocacao.model.common.EntityInterface;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author arthur.diego
 */

@Entity
@Table(name = "Turno")
public class Turno implements EntityInterface<Turno>{
    
    @Id
    @GeneratedValue
    private Long idTurno;
    
    @Column(name = "turno", nullable = false)
    private Turno_Tipo turno;
    
    @ManyToOne
    private Disponibilidade disponibilidade;

    public Turno() {
    }

    public Long getIdTurno() {
        return idTurno;
    }

    public void setIdTurno(Long idTurno) {
        this.idTurno = idTurno;
    }

    public Turno_Tipo getTurno() {
        return turno;
    }

    public void setTurno(Turno_Tipo turno) {
        this.turno = turno;
    }

    public Disponibilidade getDisponibilidade() {
        return disponibilidade;
    }

    public void setDisponibilidade(Disponibilidade disponibilidade) {
        this.disponibilidade = disponibilidade;
    }

    @Override
    public String getLabel() {
        return this.getId()+" - "+this.getTurno().getLabel();
    }

    @Override
    public Long getId() {
        return this.getIdTurno();
    }

    @Override
    public int compareTo(Turno o) {
        return this.getIdTurno().compareTo(o.getIdTurno());
    }

    @Override
    public String toString() {
        return "Turno{" + "idTurno=" + idTurno + ", turno=" + turno + ", disponibilidade=" + disponibilidade + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.idTurno);
        hash = 67 * hash + Objects.hashCode(this.turno);
        hash = 67 * hash + Objects.hashCode(this.disponibilidade);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Turno other = (Turno) obj;
        if (!Objects.equals(this.idTurno, other.idTurno)) {
            return false;
        }
        if (this.turno != other.turno) {
            return false;
        }
        if (!Objects.equals(this.disponibilidade, other.disponibilidade)) {
            return false;
        }
        return true;
    }

    
    
}
