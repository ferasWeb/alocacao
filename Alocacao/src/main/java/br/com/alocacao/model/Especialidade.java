/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alocacao.model;

import br.com.alocacao.model.common.EntityInterface;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author arthur.diego
 */
@Entity
@Table(name = "Especialidade")
public class Especialidade implements EntityInterface<Especialidade> {

    @Id
    @GeneratedValue
    private Long idEspecialidade;

    @Column(name = "especialidade", nullable = false)
    private String especialidade;

    public Especialidade() {
    }

    public Long getIdEspecialidade() {
        return idEspecialidade;
    }

    public void setIdEspecialidade(Long idEspecialidade) {
        this.idEspecialidade = idEspecialidade;
    }

    public String getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(String especialidade) {
        this.especialidade = especialidade;
    }

    @Override
    public String getLabel() {
        return this.getIdEspecialidade() + " - " + this.getEspecialidade();
    }

    @Override
    public Long getId() {
        return this.getIdEspecialidade();
    }

    @Override
    public int compareTo(Especialidade o) {
        return this.getIdEspecialidade().compareTo(o.getIdEspecialidade());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.idEspecialidade);
        hash = 79 * hash + Objects.hashCode(this.especialidade);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Especialidade other = (Especialidade) obj;
        if (!Objects.equals(this.idEspecialidade, other.idEspecialidade)) {
            return false;
        }
        if (!Objects.equals(this.especialidade, other.especialidade)) {
            return false;
        }
        return true;
    }

}
