package br.com.alocacao.model;

import br.com.alocacao.enumerated.Disciplina_Tipo;
import br.com.alocacao.model.common.EntityInterface;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Sala")
public class Sala implements EntityInterface<Sala> {

    @Id
    @GeneratedValue
    private Long idSala;

    @Column(name = "nome_sala", nullable = true, length = 100, unique = true)
    private String nomeSala;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_aula", nullable = false)
    private Disciplina_Tipo tipo_aula;

    public Sala() {
    }

    public Long getIdSala() {
        return idSala;
    }

    public void setIdSala(Long idSala) {
        this.idSala = idSala;
    }

    public String getNomeSala() {
        return nomeSala;
    }

    public void setNomeSala(String nomeSala) {
        this.nomeSala = nomeSala;
    }

    public Disciplina_Tipo getTipo_aula() {
        return tipo_aula;
    }

    public void setTipo_aula(Disciplina_Tipo tipo_aula) {
        this.tipo_aula = tipo_aula;
    }

    @Override
    public String getLabel() {
        return this.getId() + " - " + this.getNomeSala();
    }

    @Override
    public Long getId() {
        return this.getIdSala();
    }

    @Override
    public int compareTo(Sala o) {
        return this.getIdSala().compareTo(o.getIdSala());
    }

    @Override
    public String toString() {
        return "Sala{" + "idSala=" + idSala + ", nomeSala=" + nomeSala + ", tipo_aula=" + tipo_aula + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.idSala);
        hash = 43 * hash + Objects.hashCode(this.nomeSala);
        hash = 43 * hash + Objects.hashCode(this.tipo_aula);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sala other = (Sala) obj;
        if (!Objects.equals(this.idSala, other.idSala)) {
            return false;
        }
        if (!Objects.equals(this.nomeSala, other.nomeSala)) {
            return false;
        }
        if (this.tipo_aula != other.tipo_aula) {
            return false;
        }
        return true;
    }

}
