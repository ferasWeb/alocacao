/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.alocacao.model.common;

import java.io.Serializable;

/**
 *
 * @author arthur.diego
 */
public interface EntityInterface <T> extends Comparable<T>, Serializable{
    
    String getLabel();
    
    Long getId();
}
