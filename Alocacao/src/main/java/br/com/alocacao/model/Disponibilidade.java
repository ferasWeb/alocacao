/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alocacao.model;

import br.com.alocacao.enumerated.Dia_Tipo;
import br.com.alocacao.model.common.EntityInterface;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author arthur.diego
 */
@Entity
@Table(name = "Disponibilidade")
public class Disponibilidade implements EntityInterface<Disponibilidade>{

    @Id
    @GeneratedValue
    private Long idDisponibilidade;
    
    @Enumerated(EnumType.STRING)
    private Dia_Tipo dia;
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "disponibilidade")
    private List<Turno> turno;
    
    @ManyToOne
    private Professor professor;

    public Long getIdDisponibilidade() {
        return idDisponibilidade;
    }

    public void setIdDisponibilidade(Long idDisponibilidade) {
        this.idDisponibilidade = idDisponibilidade;
    }

    public Dia_Tipo getDia() {
        return dia;
    }

    public void setDia(Dia_Tipo dia) {
        this.dia = dia;
    }

    public List<Turno> getTurno() {
        return turno;
    }

    public void setTurno(List<Turno> turno) {
        this.turno = turno;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    @Override
    public String getLabel() {
        return "";
    }

    @Override
    public Long getId() {
        return this.getIdDisponibilidade();
    }

    @Override
    public int compareTo(Disponibilidade o) {
        return this.getIdDisponibilidade().compareTo(o.getIdDisponibilidade());
    }

    @Override
    public String toString() {
        return "Disponibilidade{" + "idDisponibilidade=" + idDisponibilidade + ", dia=" + dia + ", turno=" + turno + ", professor=" + professor + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.idDisponibilidade);
        hash = 59 * hash + Objects.hashCode(this.dia);
        hash = 59 * hash + Objects.hashCode(this.turno);
        hash = 59 * hash + Objects.hashCode(this.professor);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Disponibilidade other = (Disponibilidade) obj;
        if (!Objects.equals(this.idDisponibilidade, other.idDisponibilidade)) {
            return false;
        }
        if (this.dia != other.dia) {
            return false;
        }
        if (!Objects.equals(this.turno, other.turno)) {
            return false;
        }
        if (!Objects.equals(this.professor, other.professor)) {
            return false;
        }
        return true;
    }

    
    
   

}
