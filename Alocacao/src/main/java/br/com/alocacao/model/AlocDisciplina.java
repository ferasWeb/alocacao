/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alocacao.model;

import br.com.alocacao.model.common.EntityInterface;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author arthur.diego
 */
@Entity
@Table(name = "Aloc_Disciplina")
public class AlocDisciplina implements EntityInterface<AlocDisciplina> {

    @Id
    @GeneratedValue
    private Long idAlocDisciplina;

    @ManyToOne(fetch = FetchType.EAGER)
    private Curso curso;

    @ManyToOne(fetch = FetchType.EAGER)
    private Semestre semestre;

    @ManyToOne(fetch = FetchType.EAGER)
    private Disciplina disciplina;

    public AlocDisciplina() {
    }

    public Long getIdAlocDisciplina() {
        return idAlocDisciplina;
    }

    public void setIdAlocDisciplina(Long idAlocDisciplina) {
        this.idAlocDisciplina = idAlocDisciplina;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public Semestre getSemestre() {
        return semestre;
    }

    public void setSemestre(Semestre semestre) {
        this.semestre = semestre;
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    @Override
    public String getLabel() {
        return this.getIdAlocDisciplina() + " - " + this.getDisciplina().getNomeDisciplina() + " - " + this.getCurso().getCurso_nome()
                + " - " + this.getSemestre().getSemestre().getLabel();
    }

    @Override
    public Long getId() {
        return this.getIdAlocDisciplina();
    }

    @Override
    public int compareTo(AlocDisciplina o) {
        return this.getIdAlocDisciplina().compareTo(o.getIdAlocDisciplina());
    }

    @Override
    public String toString() {
        return "AlocDisciplina{" + "idAlocDisciplina=" + idAlocDisciplina + ", curso=" + curso + ", semestre=" + semestre + ", disciplina=" + disciplina + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.idAlocDisciplina);
        hash = 79 * hash + Objects.hashCode(this.curso);
        hash = 79 * hash + Objects.hashCode(this.semestre);
        hash = 79 * hash + Objects.hashCode(this.disciplina);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlocDisciplina other = (AlocDisciplina) obj;
        if (!Objects.equals(this.idAlocDisciplina, other.idAlocDisciplina)) {
            return false;
        }
        if (!Objects.equals(this.curso, other.curso)) {
            return false;
        }
        if (!Objects.equals(this.semestre, other.semestre)) {
            return false;
        }
        if (!Objects.equals(this.disciplina, other.disciplina)) {
            return false;
        }
        return true;
    }

}
