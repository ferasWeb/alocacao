/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alocacao.enumerated;

/**
 *
 * @author arthur.diego
 */
public enum Disciplina_Tipo {

    P("Pratica"),
    T("Teorica");
    private String label;

    private Disciplina_Tipo(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
