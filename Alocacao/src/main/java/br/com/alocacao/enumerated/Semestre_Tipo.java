/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alocacao.enumerated;

/**
 *
 * @author arthur.diego
 */
public enum Semestre_Tipo {

    PRIMEIRO("1º"),
    SEGUNDO("2º"),
    TERCEIRO("3º"),
    QUARTO("4º"),
    QUINTO("5º");

    String label;
    
    private Semestre_Tipo(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

   

}
