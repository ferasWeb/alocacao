/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alocacao.enumerated;

/**
 *
 * @author arthur.diego
 */
public enum Turno_Tipo {

    M("Diurno"),
    N("Noturno");
    private String label;

    private Turno_Tipo(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
