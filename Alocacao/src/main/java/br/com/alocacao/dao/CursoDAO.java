/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.alocacao.dao;

import br.com.alocacao.dao.common.AbstractEntityBeans;
import br.com.alocacao.model.Curso;
import br.com.alocacao.utils.UtilBeans;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author arthur.diego
 */
@Stateless
@LocalBean
public class CursoDAO extends AbstractEntityBeans<Curso, Long>{

    @PersistenceContext(unitName = UtilBeans.PERSISTENCE_UNIT) 
    private EntityManager entityManager;

    public CursoDAO() {
        super(Curso.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }
    
}
