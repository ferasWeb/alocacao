/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alocacao.dao.common;

import br.com.alocacao.model.common.EntityInterface;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author arthur.diego
 */
public abstract class AbstractEntityBeans<T extends EntityInterface, ID extends Serializable> {

    protected abstract EntityManager getEntityManager();

    private final Class<T> entityClass;

    public AbstractEntityBeans(final Class<T> entityClass) {
        this.entityClass = entityClass;

    }

    public void save(T entity) throws Exception {
        try {

            getEntityManager().persist(entity);

        } catch (Exception e) {
            throw new Exception(e.getCause());
        }
    }

    public void update(T entity) throws Exception {
        try {

            getEntityManager().merge(entity);

        } catch (Exception e) {

            throw new Exception(e.getCause());
        }
    }

    public void delete(T entity) throws Exception {
        try {
            getEntityManager().remove(getEntityManager().merge(entity));
            getEntityManager().flush();
        } catch (Exception e) {

            throw new Exception(e.getCause());
        }
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq
                = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public T find(final ID id) {
        try {
            return getEntityManager().find(entityClass, id);
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<T> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        Query q = getEntityManager().createQuery(cq);
        /**
         * Recebe no máximo o resultado que foi instipulado no entityPagination
         */
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}
