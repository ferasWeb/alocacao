
package br.com.alocacao.dao;

import br.com.alocacao.dao.common.AbstractEntityBeans;
import br.com.alocacao.model.Sala;
import br.com.alocacao.utils.UtilBeans;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
@LocalBean
public class SalasDAO extends AbstractEntityBeans<Sala, Long>{

    @PersistenceContext(unitName = UtilBeans.PERSISTENCE_UNIT)
    private EntityManager em;
    
    public SalasDAO() {
        super(Sala.class);
    }

    @Override
    protected EntityManager getEntityManager() {

        return em;
    }

    
    

}
