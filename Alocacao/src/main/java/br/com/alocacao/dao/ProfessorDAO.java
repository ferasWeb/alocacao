
package br.com.alocacao.dao;

import br.com.alocacao.dao.common.AbstractEntityBeans;
import br.com.alocacao.model.Professor;
import br.com.alocacao.utils.UtilBeans;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
@LocalBean
public class ProfessorDAO extends AbstractEntityBeans<Professor, Long>  {

    @PersistenceContext(unitName = UtilBeans.PERSISTENCE_UNIT)
    private EntityManager em;

    public ProfessorDAO() {
        super(Professor.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
    
    
}
