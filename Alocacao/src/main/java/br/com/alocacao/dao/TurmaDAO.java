
package br.com.alocacao.dao;

import br.com.alocacao.dao.common.AbstractEntityBeans;
import br.com.alocacao.model.Turma;
import br.com.alocacao.utils.UtilBeans;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
@LocalBean
public class TurmaDAO extends AbstractEntityBeans<Turma, Long>{

    @PersistenceContext(unitName = UtilBeans.PERSISTENCE_UNIT)
    private EntityManager em;
    
    public TurmaDAO() {
        super(Turma.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    
    
    
    
    
}
