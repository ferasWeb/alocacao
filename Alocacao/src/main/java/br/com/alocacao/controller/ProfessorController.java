/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alocacao.controller;

import br.com.alocacao.model.Professor;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author FATE - Aluno
 */
@ManagedBean
@ViewScoped
public class ProfessorController {

    private Professor professor;
    private List<Professor> listProfessor;

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public List<Professor> getListProfessor() {
        return listProfessor;
    }

    public void setListProfessor(List<Professor> listProfessor) {
        this.listProfessor = listProfessor;
    }

}
