/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alocacao.controller;

import br.com.alocacao.controller.common.EntityController;
import br.com.alocacao.controller.common.EntityPagination;
import br.com.alocacao.dao.CursoDAO;
import br.com.alocacao.model.Curso;
import br.com.alocacao.utils.JsfUtil;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

/**
 *
 * @author FATE - Aluno
 */
@ManagedBean
@ViewScoped
public class CursoController extends EntityController<Curso> {

    private Curso current;
    @EJB
    private CursoDAO dao;
    private EntityPagination pagination;
    private int selectedItemIndex;
    private boolean enableBtnAdd = false;
    private boolean enableFormCurso = true;

    public String enableOrDisableBtn(){
       this.setEnableFormCurso(false);
       this.setEnableBtnAdd(true);
       return JsfUtil.MANTEM;
    }
    
    public String enableOrDisableForm() {
        this.setEnableBtnAdd(false);
        this.setEnableFormCurso(true);
        System.out.println(this.enableBtnAdd);
        System.out.println(this.enableFormCurso);
        return JsfUtil.MANTEM;
    }

    public boolean isEnableBtnAdd() {
        return enableBtnAdd;
    }

    public void setEnableBtnAdd(boolean enableBtnAdd) {
        this.enableBtnAdd = enableBtnAdd;
    }

    public boolean isEnableFormCurso() {
        return enableFormCurso;
    }

    public void setEnableFormCurso(boolean enableFormCurso) {
        this.enableFormCurso = enableFormCurso;
    }
    
    public Curso getCurrent() {
        return current;
    }

    public void setCurrent(Curso current) {
        this.current = current;
    }

    private CursoDAO getDao() {
        return dao;
    }

    @Override
    protected void setEntity(Curso t) {
        this.current = t;
    }

    @Override
    protected Curso getNewEntity() {
        return new Curso();
    }

    @Override
    protected void performDestroy() {
        try {
            dao.delete(current);
            // JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("EntidadeDeleted"));
        } catch (Exception e) {
            //  JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    @Override
    protected String create() {
        try {
            getDao().save(current);
            recreateTable();
            //JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("EntidadeCreated"));
            return clean();
        } catch (Exception e) {
            //  JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    @Override
    protected String update() {
        try {
            getDao().update(current);
            recreateTable();
            // JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("EntidadeUpdated"));
            return JsfUtil.MANTEM;
        } catch (Exception e) {
            //  JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    @Override
    public EntityPagination getPagination() {
        if (pagination == null) {
            pagination = new EntityPagination(9) {
                /**
                 * Deve informar a quantidade de itens da tabela, se houver
                 * filtros estes devem ser utilizados na consulta de contagem
                 * também.
                 *
                 * @return Quantidade de itens total.
                 */
                @Override
                public int getItemsCount() {
                    return getDao().count();
                }

                /**
                 * Cria o DataModel que terá os item da página exibida. Preenche
                 * a quantidade instipulada no entityPagination nesse caso foi
                 * de 10 itens
                 *
                 * @return DataModel com itens da página atual.
                 */
                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(dao.findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return this.pagination;
    }

}
